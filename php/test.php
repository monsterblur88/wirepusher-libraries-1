<?php

include_once('wirepusher.php');

$title = 'I am an encrypted title';
$message = 'You can make it anything happen!';

list ($http_status, $response) = Wirepusher::send('id_here', $title, $message, 'optional_type', 'optional_password_here');

echo 'HTTP Code: ' . $http_status . PHP_EOL;
echo $response . PHP_EOL;